#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Script.

Usage: python main.py [<paths/to/data.files>, ...]

About:

- you will be given lat,lng points
- you will be given a buffer
- you will need to provide a convex hull
"""


import json
import logging
import typing

from scipy.spatial import ConvexHull


Point: typing.TypeAlias = tuple[float, float]

ExifJson: typing.TypeAlias = dict[str, dict[str, float]]
MapGeoJson: typing.TypeAlias = dict[
    str, list[dict[str, dict[str, list[list[list[float]]]]]]
]
MapBufferGeoJson: typing.TypeAlias = dict[
    str, list[dict[str, dict[str, list[list[list[list[float]]]]]]]
]


class ConvexHullGenerator:
    """
    Convex hull generator.
    """

    points: list[Point]
    buffer: list[Point]

    def __init__(self) -> None:
        self.points = []
        self.buffer = []

    def add_from_exif_json(self, exif_json: ExifJson) -> None:
        """
        Add points from the given EXIF JSON data.

        Args:
            exif_json (ExifJson): The EXIF JSON data to add the points from.
        """

        self.points.extend(
            (image_data["latitude"], image_data["longitude"])
            for image_data in exif_json.values()
            if isinstance(image_data, dict)
            and "latitude" in image_data
            and "longitude" in image_data
        )

    def add_from_map_geojson(self, map_geojson: MapGeoJson) -> None:
        """
        Add points from the given Map GeoJSON data.

        Args:
            map_geojson (MapGeoJson): The Map GeoJSON data to add the points from.
        """

        if "features" not in map_geojson:
            logging.warning("No features found in Map GeoJSON.")
            return

        self.points.extend(
            (coordinates[0], coordinates[1])
            for feature in map_geojson["features"]
            if isinstance(feature, dict)
            and "geometry" in feature
            and isinstance(feature["geometry"], dict)
            and "coordinates" in feature["geometry"]
            for coordinates_set in feature["geometry"]["coordinates"]
            for coordinates in coordinates_set
        )

    def add_from_map_buffer_geojson(self, map_buffer_geojson: MapBufferGeoJson) -> None:
        """
        Add buffer from the given Map Buffer GeoJSON data.

        Args:
            map_buffer_geojson (MapBufferGeoJson): The Map Buffer GeoJSON data to add the buffer from.
        """

        if "features" not in map_buffer_geojson:
            logging.warning("No features found in Map GeoJSON.")
            return

        self.buffer.extend(
            (coordinates[0], coordinates[1])
            for feature in map_buffer_geojson["features"]
            if isinstance(feature, dict)
            and "geometry" in feature
            and isinstance(feature["geometry"], dict)
            and "coordinates" in feature["geometry"]
            for coordinates_sets in feature["geometry"]["coordinates"]
            for coordinates_set in coordinates_sets
            for coordinates in coordinates_set
        )

    def add_from_data_files(self, data_file_paths: typing.Iterable[str]) -> None:
        """
        Add points from data files at the given paths.

        Args:
            data_file_paths (typing.Iterable[str]): Paths to data files.
        """

        for data_file_path in data_file_paths:
            self.add_from_data_file(data_file_path)

    def add_from_data_file(self, data_file_path: str) -> None:
        """
        Add points from data file at the given path.

        Args:
            data_file_path (str): Path to data file.
        """

        EXIF_JSON_FILE_SUFFIX = "exif.json".casefold()
        MAP_GEOJSON_FILE_SUFFIX = "map.geojson".casefold()
        MAP_BUFFER_GEOJSON_FILE_SUFFIX = "map_buffer.geojson".casefold()

        with open(data_file_path, "r", encoding="utf-8") as data_file:
            data = data_file.read()

        match data_file_path.casefold():
            case exif_json_file_path if exif_json_file_path.endswith(
                EXIF_JSON_FILE_SUFFIX
            ):
                logging.info("Adding points from EXIF JSON file: %s", data_file_path)

                exif_json: ExifJson = json.loads(data)
                self.add_from_exif_json(exif_json)

            case map_geojson_file_path if map_geojson_file_path.endswith(
                MAP_GEOJSON_FILE_SUFFIX
            ):
                logging.info("Adding points from Map GeoJSON file: %s", data_file_path)

                map_geojson: MapGeoJson = json.loads(data)
                self.add_from_map_geojson(map_geojson)

            case map_buffer_geojson_file_path if map_buffer_geojson_file_path.endswith(
                MAP_BUFFER_GEOJSON_FILE_SUFFIX
            ):
                logging.info(
                    "Adding buffer from Map Buffer GeoJSON file: %s", data_file_path
                )

                map_buffer_geojson: MapBufferGeoJson = json.loads(data)
                self.add_from_map_buffer_geojson(map_buffer_geojson)

            case _:
                logging.warning("Skipping unsupported file: '%s'", data_file_path)

    def generate(self) -> ConvexHull:
        """
        Generates the convex hull.

        Returns:
            ConvexHull: The generated convex hull.
        """

        return ConvexHull(self.points)


def main(argv: list[str]) -> int:
    """
    Entrypoint.
    """

    DEFAULT_DATA_FILES = ["exif.json", "map.geojson", "map_buffer.geojson"]
    data_files = argv[1:] if len(argv) >= 2 else DEFAULT_DATA_FILES
    logging.info("Received data files: %s", data_files)

    convex_hull_generator = ConvexHullGenerator()
    convex_hull_generator.add_from_data_files(data_files)

    return 0


if __name__ == "__main__":
    import sys

    code = main(sys.argv)
    sys.exit(code)
